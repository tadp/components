define(['selector/qwery', 'events/bean','./databind'],function($, _, directive){
		
	return function(scope, target){
			//1. set any data into 'scope' to bind to the html
			scope.message= "This line is set dynamically in JS (with data-binding) and displayed at 1.5 secs.";
			
			//arg2 is optional. it indicates whether to use auto-binding or not.
			scope.bind(directive, false);
			
			//2. Bind events for interaction with users OR components
			//uses delegated events, set on the 'target' element. that way, if some HTML in this component is re-rendered, we won't lose the events bound to the nodes in the removed HTML.
			_.on(target,'.actions input','click',function(e){
				alert("selected: "+ this.value); //ok or cancel
				_.fire(target, 'close');
			});
			
		
	}
	
});
