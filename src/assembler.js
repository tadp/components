/* This module loads and assembles components to construct dynamic views in web-applications. */

define(['selector/qwery','events/bean','jquery','timesheets/main','databind/rendering','domReady!'],function($, _, dom,ts,db){
	var pathsMap= {},
		tmpDiv= document.createElement('div'),
		docFrag= document.createDocumentFragment();	
		
	//create stylesheet used to insert styles of components. I associate our stylesheet with the title STYLE_ID. it will insert a new stylesheet if none found with the given title. why not use ID attr? coz the HTML standard doesn't define an ID attr for <style> elemnts.
	var mystyle= dom('<style/>', { //build the css style node
				type:   'text/css',
				title:  'views-style' //STYLE_ID
	    	  }).prependTo(document.head)[0]; //and append it to the head
	    	  	  
	if(!window.createPopup)  //for Safari
		mystyle.appendChild(document.createTextNode('')); //mystyle.html('')
		
	var rulesLen= mystyle.sheet.cssRules.length;
	//TODO: if standard Stylesheet API is not supported (ie. IE<9), expand style node, eg. use fixStyleSheetAPI() in /lib/hallo/stylesheets.js. Why not use a polyfill? coz we can't modify host objects, so when we create a new <style> via documentCreate('style'), or when the browser creates it via innerHTML, the style node won't have the standard API added automatically, so we need to add it after the style object has been created.
	
	function isAbsPath(p){
		return ((/^https?:/i).test(p) || p[0]==="/"); 
	}
	
	function resolvePath(basePath, path){
		
		//if path is absolute, return path
		if(isAbsPath(path))	return path;
		
		//else, prepend basePath to path and return it
		if(path[0]!=="."){
			var tokens= path.split('/',1);
			var match= pathsMap[tokens[0]];
			if(match){ 
				//TODO: make sure that 'match' ends in '/' IF it defines a *partial* path (ie. tokens[1] is not empty), eg. if 'match' is "jquery" and points to 'somepath/jquery.js' then no need to have '/' at the end; so, only add '/' at the end when tokens[1] is not undefined
				basePath= match;
				path= (tokens.length>1)? tokens[1]: '';
			}
		} 
		return basePath + path;
	}
	
	function findPaths(doc, basePath){
		var meta= $('meta[name=paths]', doc)[0];
		if(!meta) return;
		var paths= meta.getAttribute('content').split(',');
		for(var i=0, l=paths.length; i<l; i++){
			var path= paths[i].split(/\s*:\s*/),
			//path[0] is pathId (ie. alias), path[1] is the url
			resId= path[0].replace(' ',''); //resourceId. trim leading white space
			// insert new paths into Curl's config. Edit: I store them in a hashtable in this module, not in Curl's config; this way I can query the hashtable in resolvePath() 
			if(resId){ 
				var resPath= path[1].replace(/\s*$/g,'');
				pathsMap[resId]= (isAbsPath(resPath))? resPath: basePath+resPath;
			}
		}
	}
	function postProcessView(basePath,target, elm, tdefs, renderer){
			//checks if 'target' node has child nodes; if so, check if 'elm' has <content> element, and loop thru them: if it has a 'select' attr -> get the matching nodes from target's childNodes and replaceWith this <content>. 
			//TODO: should I copy <content>'s attrs to the node that replaces it?? eg. style or class attributes?? yes, i think so.
			//TODO: cache target's 'contents', in case the template is rendered multiple times, because we'd lose them once we attach our template into 'target' node. Edit:Think if it's better to add contents to the template not AFTER the html is rendered but BEFORE the templating engine caches the template (??)
			
			var contents= dom(target).children(); //why not use target.childNodes? coz childNodes includes text nodes, and then i'd have to filter them, while children() automatically filters them 
			if(contents.length){
				var attachPoints=  $('content', elm),
					contentParent= target; 
				for(var i=0; i< attachPoints.length;i++){
					var ap= attachPoints[i], sel= ap.getAttribute('select');
					if(sel){
						//Warning: i'm assuming that 'select' matches only *one* node (that's why i added "[0]" ), not multiple nodes. Otherwise, i'll have to modify the line below, using jQuery's method to append multiple nodes, not only $(sel, contentParent)[0] 
						dom(ap).replaceWith( $(sel, contentParent)[0] );
					}else{
						dom(ap).replaceWith( contentParent.childNodes );
						contentParent= ap.parentNode;
					}
				}
				
			}			
			
			//attach result dom tree to target. I *replace* target's content, not *append* to it, coz this might be a second time that we add html to target as result of re-rendering data into template.
			//TODO: should i append template's *childNodes* instead of template node? i think so, because 'elm' is a <div> (which corresponds to <element> in the Web Components proposal), and inside it has the <template>, so i should not attach neither <element> node nor <template> node to the "target"s node.
			//  append(elm.childNodes ??)?? Edit: the browser does a repaint after each child node is attached to DOM, thus it'd be faster appending just one node (ie. 'elm' node), ie. a div that has the child nodes, which is the template's root [Option 2]. Edit: a DocumentFragment solves this problem: first add all the nodes to a docFrag, and then do target.appendChild(docFrag) and all the docFrag's child nodes will be appended to DOM at once, without the need to loop and append docFrag's child nodes one by one, this way the browser only does one repaint [Option 1]. 

			//[Option 1]: Add elm's children to target instead of elm:
			//Tip: To loop through a childNodes list, it is more efficient to use the nextSibling property than to explicitly use the childNodes list of the parent object. Note, first insert all nodes in a list, coz when a node is appended to docFrag its nextSibling becomes null.
			var r=[], n= elm.firstChild;
			for( ; n; n=n.nextSibling)
				if(n.nodeType <4) r.push(n); //includes text nodes	
			for(var i=0, l= r.length; i<l; i++)
				docFrag.appendChild(r[i]);
		 	
			
			//how to remove a node's children? see http://stackoverflow.com/questions/3955229/remove-all-child-elements-of-a-dom-node-in-javascript
			dom(target).empty();
			target.appendChild(docFrag);
			
			//[Option 2]: Add 'elm' directly to target, instead of only elm's children.
			//dom(target).empty().append(elm);
			
			_.fire(target, "HTMLContentInserted");
			
			//Load timesheets. It's done before calling databind(), because timesheets affects presentation, while databind doesn't affect immediately, so user sees the rendered results immediately.
			tdefs.length && ts.load( tdefs , target );
			renderer && renderer.databind();
			
			//TODO: [x]after the elm has been added to target node, scan the newly rendered DOM tree to look for subviews, ie. call findViews( target, basePath ). Note, i must pass basePath to findViews(), so that modules defined in subviews can be resolved by the loader
			findViews(target, basePath );
			
	}
	//Warning: when we parse a subview's HTML into a <div>, browsers don't parse <html>,<head>, <body> tags, so don't use those tags in my selector queries!! Also, IE and Safari (and Midori) don't even parse the contents of <head> at all!! it doesn't work even if I use an <html> element to insert our html string. Solution? see the comments on this issue in my notes of this module. ATM, the fastest approach I know is to remove the conflicting tags (using a regex 'matchTag') from htmlText and parse the result into tmpDiv. There is another option but it's not as fast as what i implemented now.
	var matchTag = /<(\/?)(html|head|body)(\s+[^>]*)?>/ig;
	function loadView(target, viewPath){
		var basePath= viewPath.substr(0,viewPath.lastIndexOf("/")+1);
		
		//should I create a plugin (eg. "html!") to load + parse html?
		curl(["text!"+viewPath]).then(function(htmlTxt){
			htmlTxt = htmlTxt.replace(matchTag,'');
			tmpDiv.innerHTML= htmlTxt;
			parseView(target, tmpDiv, basePath);
		},function(){ //in case of error
			//TODO: no need to use an error callback here, remove it and use a global error callback, which will simply print an error msg saying the URL that could not be loaded.
		}); 
	}
	
	function parseStylesheets(scopeId, html, basePath){
		
		function addStylesFromText(cssTxt, scopeId){
			//styles are scoped to the component in which are defined: get target's id (if it hasn't one, assign it a random number), and get the styles as an array of rules, and then prepend target's id to each rule in array, and insert in <style> used to add components styles. Edit: using an Id to scope the CSS rules gives problems to overwrite them in global styles (see [issue #2]), so I changed the approach and use a 'data-id' attr.

			//remove comments in CSS text
			cssTxt= cssTxt.replace(/\/\*(?:(?!\*\/)[\s\S])*\*\//g,'');

			var cssList= cssTxt.split(/[{}]/ ); 
			//Note: split('}' ) includes an empty item into cssList[], that's why I exclude it in the loop below (ie. end loop at "cssList.length - 1" )
			for(var i=0, l= cssList.length-1;i< l;i+=2){
				//prepend scopeId to selector of each rule. see [issue #2] 
				
				//when having multiple selectors in LHS of the CSS rule, i must split them at the commas ',' and prepend the scopeId to each of the parts, to scope each individual selector. 
				var lhs= cssList[i], rhs= cssList[i+1], //Left/Right Hand Side of CSS rule
					prefix= '[data-id='+scopeId+"] ";
				lhs = prefix+ lhs.replace(',', ','+prefix );
				
				try{	
					//mystyle.sheet.insertRule('#'+scopeId+" "+cssList[i]+'}', rulesLen++);
					mystyle.sheet.insertRule(lhs +'{'+rhs+'}', rulesLen++);
				//if couldn't insert rule but rulesLen was incremented +1, in catch() i must decrement -1; otherwise, any subsequent attempt to insert a rule will fail, because the index (rulesLen) passed to insertRule() is not in the array of rules (ie. rulesLen>mystyle.sheet.cssRules.length).
				}catch(e){
					rulesLen--;
					console.error(e); 
				} 
			}
		}
		
		var links= $('link[rel=stylesheet]', html);		
		for(var i=0;i<links.length;i++){
			//Warning: don't use links[i].href *property* to get the URL, because the browser automatically added a base path to the href *attribute*, eg. if the attribute value is href="./style.css", the property href will be "http://localhost:8000/components/tests/style.css", using as base path the initial html file (test1.html), not the html of the subview ( /dialog/template.html). solution: use the href *attribute* (ie. links[i].getAttribute()) and prepend our basePath. 
			
			//TODO: [BUG] if i pass path "dialog/style.css", Curl translates it into "baselibs/dialog/style.css", which means that it's using the basePath defined in Curl's config. However, if I used a path "../components/tests/dialog/style.css" Curl converts it to "http://localhost:8000/components/src/tests/dialog/style.css", What's going on???!!! something is wrong in Curl!! [Note: when calling "curl(  )", if you use relative paths (eg. "../blahblah" ), it can't be relative to *this* JS file, because Curl doesn't know where that curl() call was made. The only way it has to know the JS file path is when calling "define()", that's why the *relative* paths in define(  ) are relative to the current JS file, but the relative paths in curl( ) are relative to the base path defined in Curl's config.]
//			curl(['text!../components/tests/dialog/style.css'],function(text){ //doesn't work!!
			curl(['text!'+ resolvePath(basePath, links[i].getAttribute('href')) ],function(text){
				addStylesFromText(text, scopeId);
			});
		}
			
		var styles= $('style', html);
		for(var i=0;i< styles.length;i++){
			var cssTxt = dom(styles[i]).text();
			addStylesFromText(cssTxt, scopeId);
		}
	}
	
	//TODO: Your HTML subview could also include a <title> tag if you want page titles to change in main page when the new subview is loaded.
	function parseView(target, html, basePath){
		var args= target.getAttribute("data-args"), 
			htmldeps=[]; //dependencies
		
		//1. find if there are css in the html, either external (ie. in <link>) or internal (ie. in <style>). 

		//If target node has no ID, generate a random ID. see [issue #2]
		//Option A:
		//if(!target.id)
		//	target.id= Math.floor( Math.random() * 1000000 );
		//parseStylesheets(target.id, html, basePath);
		//Option B:
		var scopeId= target.id || '_'+Math.floor( Math.random() * 1000000 );
		target.setAttribute('data-id', scopeId);
		parseStylesheets(scopeId, html, basePath);

		//2. find if there are timesheets in the html, either external (ie. in <link>) or internal (ie. in <timesheet>)
		//gets *internal* timesheets
		var timesheets = $("timesheet", html),
			tdefs= [];
		for(var i= 0, l= timesheets.length; i<l; i++)
		  tdefs= tdefs.concat( ts.xmlToJson(timesheets[i] ));
		
		//gets *external* timesheets. Since they are loaded asynchronously, and the subview's HTML depends on them, i add them to htmldeps[]
		timesheets = $("link[rel=timesheet]", html);
							
		//3. find if the component defines a JS module for 'behavior', ie. in 'data-behavior' attr. If so, load the JS module and create instance, passing 'scope' object.
		// the path in 'data-behavior' attr is relative to the subview's html file. 'basePath' is the path of this subview's html file.
		//var elm= $('body>div', html)[0]; //don't use 'body' in selector! why? see [issue #2]
		var elm= $('div', html)[0];
		//TODO: handle when 'elm' is not found in html, eg. for a mistake in markup; otherwise, elm.getAttribute() will raise an error
		var behavior= elm.getAttribute('data-behavior'); //a JS file
		behavior && htmldeps.push( resolvePath(basePath, behavior) );

		for(var i=0;i<timesheets.length;i++){
			//don't use the *property* 'href' of <link> (ie. timesheets[i].href), instead use the *attribute* value (ie. timesheets[i].getAttribute('href')). why? see comments for stylesheets, because it's the same situation with stylesheets. 
			htmldeps.push("smil!"+ resolvePath(basePath, timesheets[i].getAttribute('href')) );
		}
				
		curl(htmldeps,function(mod){
		      
		   for(var j= (behavior? 1: 0), jl=arguments.length; j<jl;j++)
				tdefs= tdefs.concat(arguments[j]);
				
		   if(behavior){				  
			   var renderer= db(elm); //right now I use 'elm' as the template's root, because in my approach i don't use <template> tags to define the template; but i might change it later, eg. i might use <div data-template> to define the template in the subview's HTML 
			   var scope={
				   //in compile() can't do 'auto-binding', because we have no data yet. Only when data is available we can do autobinding. TODO: Despite we compile the directives here, we should be able to do auto-binding too at later time when call bind()?????
				   compile:function(directive){
					   renderer.compile(directive);
						
				   },bind: function(directive, autorender){
						//in renderer.render(), arg1 is the data to render into html, but it's been stored in 'scope' obj, so i just pass the 'scope' object as arg1.
						
						//TODO: i'm going to modify templates to return one dom tree, instead of an array result[]
						var result= renderer.render(this, directive, autorender)[0];
						
						//It always does 2-way databind by default. It'll be done if arg5 is given (ie. 'renderer'). Instead, we could add an option in bind() to indicate whether to do 2-way databind or not.
						postProcessView(basePath,target, result, tdefs, renderer);
						
					} 
				}
				//'target' is the element under which the subview's HTML is going to be appended, ie. it's what defines the context/scope for the html to *constraint* css styles and Timesheets. I pass the 'target' node to the subview's instance in case it wants to use it, eg. to bind events (for event delegation, coz if the subview's html is re-rendered the events won't be lost if we attached them to 'target' node ).
				var instance= new mod( scope, target  );
				instance.scope= scope; //store it in instance, in case the controller needs it at later time
				//should I store the JS module instance in the node? yes, in case i need it
				dom.data(target, "behavior", mod);
		   }else{
				postProcessView(basePath,target, elm, tdefs);
			   }
		}); 
	}
	
	function findViews(ctx, basePath){
	
		function loadDeferedView(ev){
			loadView(this, resolvePath(basePath, this.getAttribute("data-ref")));
			_.off(this, "begin", loadDeferedView);
		}
	
		var targets= $('*[data-ref]', ctx);
		for(var i=0, l=targets.length; i<l; i++){
			var target= targets[i];
			
			var viewPath= target.getAttribute("data-ref");

			//how to defer loading a module until it is activated? eg. I don't want to load modules that are not used often by users (eg. "settings" section), so i only load them when user tries to use it. how? I add a special attr "data-defer=true" in the module instantiation node. If that attr is found and it's 'true', I attach an event handler for the event 'begin', which is fired when the Timesheet engine activates the container, so the event handler will load the module.
			//TODO: make sure that the defered view starts its time nodes when it loads. Given that its parent time container had begun when the view loads and the timing data is loaded after that moment, I'm not sure if there will be a problem, because the container won't have timeNodes[] at that time, and maybe the child nodes won't be started after the module is loaded(?)).
			var defer= target.getAttribute("data-defer");
			if(defer && defer!=='false'){
				//To save memory I use a named function(eg. loadDeferedView()) as callback of the event, instead of an 'anonymous' func; because that way we won't create a new instance of a func in each iteration.
				_.on(target, "begin", loadDeferedView);

			}else{
				loadView(target, resolvePath(basePath, viewPath));				
			}
		}
	}
	
	var baseURL= window.location.href;
	baseURL= baseURL.substr(0,baseURL.lastIndexOf("/")+1);	
	findPaths(document.head, baseURL);
	findViews(document.body, baseURL);

	return {
		loadView: loadView
	}
});
