#Composition of views to build applications

##About
Despite the advanced capabilities included in HTML5, you still can't build applications composed of small, reusable pieces in a declarative way. HTML was not designed for being constructed from subviews (like Legos). We need mechanisms to encapsulate and abstract views so we can scale to build large applications. This is what the code in this package aims to do.

It provides a way to define HTML views, and load them into an HTML document. A view component includes an HTML template, style, timesheets, and JS. And it's essential that the views can be composed using HTML. Also, it addresses other questions such as how these components communicate between each other.


##Philosophy
>"Developing a medium/large application should be like composing small, tested HTML views. That is the only way you'll be able to scale up and out the development."

>"Break your applications into small pieces. Then, assemble those testable, bite-sized pieces into your big application."

>"The more tied components are to each other, the less reusable they will be, and the more difficult it becomes to make changes to one without accidentally affecting the another."

## Self-contained components
A component is "self-contained", meaning that it should be able to live on its own. It includes:

* Stylesheets (style/layout) 
* Timesheets (time/events)
* HTML template (structure)
* JavaScript module (behavior)
* Data-binding directives
* other assets (eg. images)


##Declaring a component
A view component is declared using a `<div>` tag. Its child nodes define the view's template. I don't use unknown tags (eg. `<template>`) at the moment to be able to test views alone in browsers. The component's element can define two attributes:

* data-name

A name given to the component and used to instantiate it in the HTML. Warning: this approach can lead to name collisions. Specially, if you use components created by third parties, it's possible that their 'name' attr. are equal. 

It's not recommended to define a hardcoded 'name' in a component, to avoid name collisions. Instead, I propose to allow defining "anonymous" components, in which you load a component into the HTML using its path (ie. like in AMD modules). Also, it'd be possible to define path aliases in an application's config (explained below).

* data-behavior

Path that points to the main JS file. The path is relative to this view's HTML file. Having JS code separated allows separation of concerns. The JS code follows the [AMD module](http://www.sitepen.com/blog/2012/06/25/amd-the-definitive-source/) pattern. 

Note, the syntax of data-behavior's value is like in AMD spec, which means you can use an AMD plugin to load the module, eg. "coffee!./formview.coffee", where "coffee!" is a plugin to load CoffeeScript.

Example (template.html):
    
    :::html
    <html><body>
      <div data-name="" data-behavior="./counter">
        <!-- <template> -->
          <div>
            <button>Click me</button>
            (click count: <span class="count">0</span>)
          </div>
        <!-- </template>  -->
      </div>    
    </body></html>

## Adding behavior: AMD Modules
The component's behavior is implemented as a standard AMD module. The main advantages of using AMD modules are:

* Ability to stucture the code into modules and avoidance of need for globals/namespacing.
* Anonymous Modules, which avoids module's name collision and makes code much more portable
* Dependency management, easy definition of module's dependencies and load them asynchronously

1.Initializing module

The module is loaded when the component is instantiated. It receives two parameters in its constructor: 'scope' and 'target'. 

function(scope, target){

}

'Target' is the DOM node in which this view is appended to. 'scope' is an object used for binding data and HTML template. So, the 'scope' sits between the HTML and data model.

2.Binding data to HTML

Any data to bind to HTML template must be added to the 'scope'. Example:

    :::javascript
    scope.message="Hello world!";

Then, to bind the data in scope and render the template, call bind(). Example:

    :::javascript
    scope.bind(directive, false);

To do this, we can tell bind() these two options:

* optional, define directives to specify how our data has to be bound to the template.
* let the system to "infer" the bindings between the data and template (ie. "auto-binding")

Example (counter.js):
    
    :::javascript
    define(['jquery'],function($){   
       return function(scope, target){
          scope.count= 0;
          scope.bind(null, true);
          $(target).on('click','button',function(){
              scope.count++;
          });
      }
    });

##Adding styles and timesheets

#### StyleSheets
A view component can include styles in stylesheets. It's possible to define external stylesheets using `<link>` tags or define then inside `<style>` elements. Normally, it's recommended to put the styles in a separate file. Nonetheless, for development it's useful to use `<style>` tags for fast testing.
 
Stylesheets defined in a component are "scoped", ie. they apply only to the component's HTML. The "scope" is determined by the node in which this view is instantiated. This scoping can be taken to an arbitrary level of nesting. 

Styles in a component should define only *structural* style, not appearance style (eg. shadows, colors, etc). why? because views are reused in different applications, therefore they should not say apearance. It's better to define appearance in the app's skin css, to give consistent look & feel to views in the application. 

#### Timesheets
Additionally, a component can include SMIL Timesheets to define timing and events. It's possible to define external timesheets using `<link>` tags or define then inside `<timesheet>` elements. Normally, it's recommended to put them in a separate file. Nonetheless, for development it's useful to use `<timesheet>` tags for fast testing.

SMIL Timesheets are applied only to the "scope" of a view component, not to the whole document. That means that a view's timesheet should not target elements outside the view's scope.

SMIL Timesheet definitions in different views are composed to build a global hierarchy of time containers, such that the computation of the timeline results from that structure.


##Instantiating a component
Instantiating a view component is done using `data-ref` attribute on an element. The value indicates the path to the view definition, or the view's name if it specified a data-name. The view's template is instantiated and the nodes are pasted where the target element is, much like in [XBL](http://www.w3.org/TR/XBL/ "XML Binding Language").

However, note that in our system the aim is not to have some elements (component) to behave as a single element. Instead, in our system the *emphasis* is on the ability to **compose** views. This is the truly important feature that HTML needs to build complex apps.

Example (application.html):
    
    :::html
    <body>
      ...
      <div data-ref="path/to/example/template.html">
         <p class="cnt">Some content</p>
      </div>
    </body>

Any content inside the target element (eg. '.cnt') is passed to the view, which could distribute it in its template. Also, it's possible to pass data in the target's attributes, which the component receives.

Warning: since a view can be instantiated multiple times in an HTML document, it's not safe to use "id" attributes in elements in a view's template. 

#### Deferring the loading of views
A useful feature is that view loading can be deferred, the view won't be loaded immediately (ie. it is not requested to the server). That's indicated with the `data-defer` attribute. You could indicate the event that will trigger the loading of a deferred view. This has two benefits:

* Application loads faster which improves user experience. There is no need for a "progess bar" when the application is loading (eg. as in Gmail), because the app loads the *minimal* UI to present to the user, and the rest of sub-views can be deferred.
* Application won't load sections that won't be used by user in a session. That is, there is no point to load some sections that the user is unlikely to use in all sessions. eg. you could say only load (ie. request to server) sections at the time the user clicks on the menu item that activates the section.


##Path aliases
You could use aliases for the paths, the same way as in an AMD loader, in which in config you can set the 'paths' property. So, how do I do that in HTML? at the moment I use a <meta> tag:

    :::html
    <meta name="paths" schema="JSON" content="
	        'example': 'path/to/example/template.html',
	    ... ">

and then you can use that alias in 'data-ref' attribute to instantiate such component:

    :::html
    <div data-ref="example">
    </div>


This approach is better than using pre-defined names in the component's declaration, because this approach avoids name collision, specially important when building medium and large applications, or when using components from third parties.

-Should I define aliases in main view or in subviews too?   
At the moment it's possible to define aliases in main HTML file. Nonetheless, I think each subview should define the paths that its html uses, that way we can test that subview alone in a browser.

##Passing child nodes to a component
Using a special `<content>` tag, a component can distribute child nodes provided when instantiating it. Also, you can use the 'select' attribute (its value is a CSS selector) to target a part of content.

For instance, here is a component's template with several `<content>` elements:

    :::html
    <div data-name="myView">
	    <h1>a title</h1>
	    <content></content>
	    Greeting: <content select=".greet"></content>
	    Who: <content select=".who"></content>
    </div>

And pass a structure to the template when the component is instantiated in HTML document:
    
    :::html
    <div data-ref="myView">	
	    <p class="greet">Hello</p>
	    <p class="who">World!</p>
	    <p>This is a test:</p>
    </div>

The rendered HTML after distributing the 'content' nodes into the template is as follows:

    :::html
    <h1>a title</h1>
    <p>This is a test:</p>
    Greeting: <p class="greet">Hello</p>
    Who: <p class="who">World!</p>


Note that the rest of content that didn't match a selector is attached to the `<content>` element that doesn't specify any 'select' attribute.

##Passing data to a component
TODO

##Communication between modules
Use the event system if the module needs to communicate with others. ie. listen/trigger events. This allows building applications with loose coupled components. Components should be unaware of other components. To build highly scalable applications where pieces could be easily added/removed.

##Example
Let's see an example to show "what a component is" and "how it's instantiated":
* A modal dialog: a simple but complete example of "self-contained" component. It includes an HTML template, CSS file, Timesheets, data-binding directive, and JS module for behavior.

The dialog example is available online to see the end result:  
<http://tadp.bitbucket.org/components/tests/test1.html>

To see the source code of the example, go to the 'tests' directory:  
<https://bitbucket.org/tadp/components/src/master/tests>

