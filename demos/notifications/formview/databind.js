/* notes: 
1) in <input> (and <select>?), you don't need to specify '@value', because our system automatically maps the value to the attr 'value' in <input>. This is like with <img>, in which any value maps to its attr 'src'.
2) in loops, use '.' to access the root node in the loop template. eg. in this example, if I use 'option':'o.value' to set the option's text, the system can't find the selector 'option' (because CSS searches don't include current node).   
*/
define({
	'input.msg@value':'msg',
	'option':{
		'o<-options':{
			'@value':'o.id', //or '.@value'
			'.':'o.value'
		}
	},
	'.level@value':'level' 
});
