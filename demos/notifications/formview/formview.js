define(['lang/aug','events/bean','./databind','lang/validation'],function(aug, _, directive, cv){
	
	var data={msg:'hello planet!', options: [{"id": 1, "value":"info" }, {"id": 2, "value":"warn" }, 
                {"id": 3, "value":"error"}], level: 1};
	
	return function(scope, target){
			//1. set any data into 'scope' to bind to the html
			aug(scope, data);
			
			//arg2 is optional, it indicates whether to use auto-binding or not.
			scope.bind(directive, false);
			
			//2. Bind events for interaction with users OR components
			//uses delegated events, set on the 'target' element. that way, if some HTML in this component is re-rendered, we won't lose the events bound to the nodes in the removed HTML.
			
			//the 'submit' event is fired only if all input fields are validated
			_.on(target,'form','submit',function(e){
				console.log(scope.level+": "+ scope.msg );
				_.fire(document, 'notify', [scope.level,scope.msg]);
				e.preventDefault();
			});
			_.on(target,'.changeMsg','click',function(e){
				scope.msg= 'hey world!';
			});
			_.on(target,'.changeSel','click',function(e){
				scope.level= 3;
			});
			_.on(target,'.addItem','click',function(e){
				scope.options.push({id:4,value:'request'});
			});
			//Validation module should be loaded in main.js, in config's preloads[]
			cv.init(null, {touchSupporting:true});
		
	}		

});
