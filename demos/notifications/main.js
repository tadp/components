/* Main config. Configures the global options, such as paths, loader options, etc. */
 
// added to be able to test my code in browsers that don't support 'console', otherwise the app will simply not work. eg. in FF the 'console' object is disabled by default.
if(typeof console == 'undefined'){
	var console = {};
	console.log= console.error= console.info= console.debug= console.warn= function(){};
	window.console= console;
}

curl({
	//define aliases for easy imports of common modules. If I moved their src files to different location, I won't need to update the paths in each module in my app, only their path in here- the loader's config. 

	//Note that baseUrl is relative to index.html file, not to the loader file (eg. curl.js) 
	//baseUrl: "https://bitbucket.org/tadp/baselibs/raw/master", 
	baseUrl: "../../../baselibs",
	paths : {
		//IMPORTANT: Do not use aliases named 'css' or 'smil'. why? see notes about this issue
		//css: 'static/css',
		jquery: 'jquery/jquery-min',
		databind: '../databinding/src',
		timesheets: '../timesheets/src',
		components: '../components/src'
	},
	pluginPath: 'loader/plugin'
	//,preloads: ['lang/validation']
	
// load the main app file in the dependency list 
}, ['events/bean','selector/qwery','components/assembler'], function(ev, $){
	ev.setSelectorEngine($);
	});
	

//TODO: load the polyfills that are needed by our application. Warning: I must load them *before* other modules, such as the timesheet engine.  see https://github.com/cujojs/poly  [it seems that it loads the polyfills in Curl's config, in the 'preloads' property (?)]
