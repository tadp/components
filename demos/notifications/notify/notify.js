/* The idea is this module receives messages from other modules, and inserts a new item into the array msgs[], and automatically the UI is updated, because I define so with databind module. When a new notification div is inserted into the DOM container, the timing data is dinamically associated to that new div, because I want that the timesheet definitions are applied to nodes added to the DOM, like CSS data in Stylesheets are applied to nodes when you insert a new node into the DOM. It's difficult to implement such feature, but I think it's important to support such dynamic assignment of timing data to new DOM nodes. */
define(['events/bean','./databind'],function(_,directive){
		
	var values= ['info','warn','error'];
	
	function notify(l, m, a){
		//why do I set level to values[l-1]? coz other modules submit a number as the level, eg. 1:"info", 2:"warn", etc since it allows easier filter notifications by level. So, this module converts to the string representation because it uses a CSS class to set the style to each level. 
		this.count++;
		this.msgs.push( {level:values[l-1], msg: m, action:a} );
		
	}
	
	return function(scope, target){	
		//1. set any data into 'scope' to bind to the html
		scope.count= 0;
		scope.msgs= [];
			
		//arg2 is optional, it indicates whether to use auto-binding or not.
		scope.bind(directive, false);
			
		//2. Bind events for interaction with users OR components
		//A msg is deleted when its scheduled time ends or when user clicks on panel
		_.on(target, 'li','end click',function(){
		//	alert(databind.dataFor(this)[1] );
			scope.msgs.shift();
			scope.count--;
		});
		_.on(document, 'notify', function(l,m,a){
			console.log("notify:"+l+","+m);
			notify.call(scope, l, m, a);
		});	
		
	}
	
});
