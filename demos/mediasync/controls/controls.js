/* KNOWN ISSUES:
- Firefox doesn't return a duration for an OGG file, as result the timebar/audio length is broken, coz the audio.duration property returns Infinity. See:
https://github.com/johndyer/mediaelement/issues/310
Edit: the problem and solution is explained here:
https://developer.mozilla.org/en-US/docs/Configuring_servers_for_Ogg_media
- In Firefox, when playback finishes, and you click on time-bar, the media starts playing immediately at that point, but i prefer the media is paused on ended and requires to call play() again. Additionaly, this behavior of Firefox caused other problem with play/pause buttons: after the audio ends the play button is displayed, but if you click on time-bar the audio starts playing but the play/pause buttons are not switched, because the 'play' event was never fired by Firefox in this case (because the media was not paused automatically on ended). Fixed:
  _.on(media, 'ended', media.pause);

*/

define(['jquery','events/bean'],function($,_){

	function timeFormat(seconds){
		var i= Math.floor(seconds/60), m= i<10?"0"+i: i;
		var j= Math.floor(seconds-(m*60)), s=j<10?"0"+j:j;
		return m+":"+s;
	}

	//update Progress Bar control
	function updatebar(x, w, progress,media){
	   var duration = media.duration;
	   var position = x - progress.offset().left; //Click pos
	   var percentage = 100 * position/w;
	
	   //Check within range
	   if(percentage > 100) {
		  percentage = 100;
	   }
	   if(percentage < 0) {
		  percentage = 0;
	   }
	   //Update progress bar and video currenttime
	   progress[0].style.width= percentage+'%';
	   media.currentTime = duration * percentage / 100;
	}

	function setMouseEvents(elm, media){
		
		var timeDrag = false, w= elm.clientWidth,
			progress=$('.progress',elm);   /* Drag status */
		_.on(elm, 'mousedown', function(e){
		   timeDrag = true;
		   updatebar(e.pageX,w, progress,media);
		});
		_.on(elm,'mouseup',function(e) {
		   if(timeDrag) {
			  timeDrag = false;
			  updatebar(e.pageX, w,progress,media);
		   }
		});
		_.on(elm, 'mousemove',function(e) {
		   if(timeDrag)
			  updatebar(e.pageX, w,progress,media);
		});
	}

	return function(scope, target){
		var dur, media= $.data(target, 'options')['mediaApi'];
		
		scope.currentTime="00:00";
		scope.totalTime="00:00";
		
		//I set "auto-binding" to *true*, so the templating system will try to guess bindings to data 
		scope.bind(null, true);
		
		var loadedMetadata= function(){
			dur= this.duration;
			scope.totalTime= timeFormat(dur); 
		};
		//it's possible that the browser triggers the media events *before* we can bind the event listeners. This is known issue with HTML5 Media and the fix is to check the media's readyState:
		if(media.readyState > 0)
		  loadedMetadata.call(media);
		else
		  _.on(media, 'loadedmetadata', loadedMetadata);
		
		_.on($("#play-button", target)[0], 'click',function(e){
			if(media.ended)
				media.currentTime = 0; //is this necessary? 

			if(media.paused || media.ended)
				media.play();
			else
				media.pause();
		});

		//Fix: in Firefox, the media is not paused on ended and *auto* plays if currentTime is set earlier than end time. 
		_.on(media, 'ended', media.pause);
	
		//Warning: ".progress" element can only be found after this view's html is rendered on the document, ie. after calling scope.bind( )
		var ctime, progress= $('.progress',target);
		_.on(media, 'timeupdate', function() {
			ctime= media.currentTime;
			scope.currentTime= timeFormat(ctime); 
			progress[0].style.width= 100*ctime/dur+'%';
			
			//Chrome: bug, HTML5 audio 'ended' event doesn't fire. http://code.google.com/p/chromium/issues/detail?id=30452
			if(media.ended) _.fire(media, 'ended');
			
			//Firefox4: bug, it sets pause=true, but clicking on time-bar it starts playing automatically, even though media.paused stays as true!!
			if(media.paused) media.pause();
		});

		//TODO: replace our timeline-bar with a slider to reuse code 
		setMouseEvents( $('.timebar',target)[0], media );
	
	
	}

});
