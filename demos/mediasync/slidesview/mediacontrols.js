//load timesheet controls defined for the specified scope
define(['jquery','events/bean','components/assembler'],function($, _, as){

function createSegment(href, width, title){
	return '<a href="#'+href+'" style="width:'+width+'%" title="'+title+'"><span>&nbsp;</span></a>';
}

return function(context){
	//in load_definitions.js, we add the timeNodes of this scope to extTiming[0]
	var parentNode= context.extTiming[0];
	if(!parentNode) return;
	var timeNodes= parentNode.timeNodes;
	for(var i=0;i<timeNodes.length;i++){
		var tn= timeNodes[i], tdefs= tn.tdefs;
		if(tdefs['controls']){
			//force computeTimeNodes, because by default that func is called when the container gets activated. so, at this time time_in/out and container's dur are *undefined*, that's why i call it here
			tn.computeTimeNodes();
			
			var controlsElm= $(tdefs['controls'], context)[0];
			$.data(controlsElm, 'options', {mediaApi: tn.mediaSyncNode});
			//create the segments for timeline
			var item, items= tn.timeNodes, dur= tn.dur, segments= "";
			for(var j=0, jl= items.length;j<jl;j++){
				item= items[j];
				var width= 100*(item.time_out-item.time_in)/dur;
				// don't need to add the id to link's href, because clicking on timebar sets the media's currentTime, and automatically the items in time container get synced; thus, there is no need to activate the items using a link's href, therefore i changed the line below:
				//segments+=createSegment('/'+ (item.target.id ||''), width, item.target.title ||'');
				segments+=createSegment('', width, item.target.title ||'');
			}
			controlsElm.innerHTML= segments;
			
			//WARNING: the segments created above have right width ONLY IF total duration of timebar is the sum of duration of each timeNode. That means, if the duration of the audio is longer than the sum of timeNodes' durs, then the segments are off, ie. the begining of a segment wouldn't match with the activation of the associated timeNode. So, make sure that the audio file has the right duration.
			
			
			//Curl uses as base path what is defined in config's "basePath", which in our case is 'baselibs'. So, to call loadView() pass either full path to our subview or resolve the path (eg. a view's scope could store its base path). I chose option1 at the moment, 'components' is defined in config's "path" object.
			as.loadView(controlsElm, 'components/../demos/mediasync/controls/template.html');
			break;//don't look for more
		}
	}
	
}
});
