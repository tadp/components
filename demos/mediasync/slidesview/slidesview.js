
define(['jquery','events/bean', './mediacontrols'],function($, _, mediaControls){
		
	var columns, rows, conHeight, conWidth,
		vExpand= 90; // % vertical expand
	function setupContainers(target){
		
	    //Adds a positioning context to elements in a cell to be able to define positition:absolute
		addPositioningContext($('.cell',target));
	
		$('.container',target).each(function(index,container){
		columns= $('>div',container);	
		var cl= columns.length, cw= 100/cl;
		for(var i=0, col; i<cl ;i++){
			col= columns[i]; 
			col.colIndex= i;
			$(col).css('width',cw+'%');
			var group= $('.cell', col);
			for(var j=0; j<group.length; j++)
				group[j].colIndex=i;
		}
	
		//##Fix the img's height, to make work overflow:hidden (see Issue #3)
		//TODO: i should redo this step when the window is resized.
		conHeight= container.clientHeight;
		conWidth= container.clientWidth;
		var imgs= $('.group .content', container);
		imgs.css('height', conHeight * vExpand/100 );
		});

	}
	function addPositioningContext(elms){
		for(var i=0, l=elms.length; i<l; i++){
			var $el= $(elms[i]),
				newDiv = $("<div />", {
				"class": "wrapper",
				"css"  : {
					"height"  : "100%",
					"width"   : "100%",
					"position": "relative"
				}
			});
			$el.wrapInner(newDiv);
		}
	}
	function fixSVGResizeIssue(e){
		var svgObjs= $('svg',e.target);
		for(var i=0;i<svgObjs.length;i++){
			var svg= svgObjs[i];
			var box = svg.viewBox.baseVal;
			var ratio = box.width / box.height;
			$(svg.parentNode).css({width: conHeight*ratio, height: conHeight}); 
		}
	}
	
	var focusGroup;
	function highlightElement(elm){
		clearPrevExpand(elm);
		
		//expand horizontally
		//This line dynamically calculates hExpand value-- using the img's computed width. [Note, the value returned by width() (or clientWidth) is in pixels, while the value i need is in %. So, i need the container's total width in pixels to compute the %value of hExpand.]
		var hExpand= ($('.content',elm)[0].clientWidth / conWidth)*100;
			
		var	container= $(elm).closest('.container'),
			columns= $('>div',container);
			
		$(columns[elm.colIndex]).css('width',hExpand+'%');
		var cl= columns.length, cw= (100-hExpand)/(cl-1);
		for(var i=0; i<cl ;i++){
			if(i===elm.colIndex) continue;
			$(columns[i]).css('width',cw+'%');
		}
		if(! $(elm.parentNode).hasClass('group')) return;
		
		//expand vertically
		var group= $('.cell',elm.parentNode);
		var ch= (100-vExpand)/(group.length-1);
		$(elm.parentNode).addClass('active'); //this sets margin-left:0 for all cells inside this column, not just the cell that got activated by timesheets.
		$(elm).css('height',vExpand+'%');
		var top=$(elm).prev();		
		for(var i=0; i<group.length; i++){
			if(elm==group[i]) continue;
			$(group[i]).css('height',ch+'%');
		}
		focusGroup=group;
	}
	function clearPrevExpand(elm){
		//reset changes of deactivated cells (eg. rows height). it should go in smil "end" event
		if(!focusGroup) return;
		if(! $(elm.parentNode).hasClass('group')){ 
			var h= 100/focusGroup.length;
			$(focusGroup[0]).closest('.group').removeClass('active');
			for(var i=0; i<focusGroup.length; i++){
				$(focusGroup[i]).css('height', h+'%');
			}
			focusGroup= null;
		}
	}
	return function(scope, target){
			
			//1. set any data into 'scope' to bind to the html
			//aug(scope, data);
			
			//arg2 is optional, it indicates whether to use auto-binding or not.
			scope.bind(null, false);
			
			setupContainers(target);
			mediaControls(target);
			
			//2. Bind events for interaction with users OR components
			//uses delegated events, set on the 'target' element. that way, if some HTML in this component is re-rendered, we won't lose the events bound to the nodes in the removed HTML.
						
			_.on(target,'.cell','begin',function(e){
				//Bean.js delegated events don't work as supposed to. It's likely fixed by upgrading to newer version (v1). As quick fix, i check if event's target is a .cell node:
				if($(e.target).hasClass('cell'))
					highlightElement(this);
			});
	
			//fixes problem that SVG container doesn't resize to SVG's width
			_.on(target, '#item7 .content','HTMLContentInserted', fixSVGResizeIssue);
		
	}		

});
